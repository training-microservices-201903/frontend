package com.artivisi.training.microservice201902.frontend.controller;

import com.artivisi.training.microservice201902.frontend.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerController {

    @Autowired private InvoiceService invoiceService;

    @GetMapping("/customer/list")
    public ModelMap dataCustomer() {
        return new ModelMap().addAttribute("dataCustomer", invoiceService.ambilDataCustomer());
    }
}
